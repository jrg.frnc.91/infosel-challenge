import jwt from "jsonwebtoken";
import { NextFunction, Request, Response } from "express";
import createError from "http-errors";
import { AuthToken } from "../types";

export default (req: Request, res: Response, next: NextFunction) => {
  const authHeader = req.headers.authorization as string;
  if (!authHeader) {
    throw createError(403, "No token provided");
  }
  const [, token] = authHeader.split(" ");
  try {
    const { userId } = jwt.verify(
      token,
      process.env.JWT_SECRET as jwt.Secret
    ) as AuthToken;
    req.userId = userId;
  } catch (err) {
    throw createError(401, "Invalid token");
  }
  return next();
};
