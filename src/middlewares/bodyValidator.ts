import Joi, { Schema } from "joi";
import { NextFunction, Request, Response } from "express";

export default (schema: Schema) =>
  async function (req: Request, res: Response, next: NextFunction) {
    try {
      req.body = await schema.validateAsync(req.body);
      next();
    } catch (e) {
      if (e instanceof Joi.ValidationError) {
        res.status(400).send({ message: e.message });
      } else {
        res.status(500).send();
      }
    }
  };
