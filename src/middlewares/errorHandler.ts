import { NextFunction, Request, Response } from "express";
import createError from "http-errors";

export default (
  error: Error,
  req: Request,
  res: Response,
  next: NextFunction
) => {
  console.error(error);
  if (error instanceof createError.HttpError) {
    res.status(error.status).send({ message: error.message });
  } else {
    res.send(500);
  }
};
