export type Person = {
  nombres: string;
  apellidos: string;
  telefono?: string;
  rfc: string;
  direccion?: string;
};

export type User = {
  id?: number;
  persona_id: number;
  contrasena: string;
  estatus: boolean;
};

export type Account = {
  numero: string;
  producto_id: number;
  saldo: number;
  estado: boolean;
  nip: string;
  persona_id: number;
};

export type AuthToken = {
  iat: number;
  userId: number;
};
