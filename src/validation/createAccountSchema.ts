import Joi from "joi";

export default Joi.object({
  nombres: Joi.string().trim().required(),
  apellidos: Joi.string().trim().required(),
  rfc: Joi.string().trim().required(),
  contrasena: Joi.string().min(5).required(),
  nip: Joi.string().length(4).required(),
});
