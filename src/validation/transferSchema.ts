import Joi from "joi";

export default Joi.object({
  fromAccount: Joi.string().trim().required(),
  toAccount: Joi.string().trim().required(),
  amount: Joi.number().positive().required(),
  concept: Joi.string().trim().required(),
  operation: Joi.string().trim().required(),
});
