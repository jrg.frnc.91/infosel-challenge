import Joi from "joi";

export default Joi.object({
  rfc: Joi.string().trim().required(),
  contrasena: Joi.string().min(5).required(),
});
