import express from "express";
import createAccount from "./routes/createAccount";
import errorHandler from "./middlewares/errorHandler";
import login from "./routes/login";
import transfer from "./routes/transfer";
import swaggerDoc from "./routes/swaggerDoc";

const app = express();

app.use(express.json());
app.use(swaggerDoc);
app.use(createAccount);
app.use(login);
app.use(transfer);
app.use(errorHandler);

export default app;
