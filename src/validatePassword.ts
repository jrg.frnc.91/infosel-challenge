import createError from "http-errors";
import bcrypt from "bcryptjs";
import getUserPass from "./data/getUserPass";

export default async (rfc: string, pass: string): Promise<void> => {
  const hashedPass = await getUserPass(rfc);
  if (!(await bcrypt.compare(pass, hashedPass))) {
    throw createError(400, "Wrong user or password");
  }
};
