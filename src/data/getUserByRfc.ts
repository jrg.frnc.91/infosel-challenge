import db from "./db";
import { User } from "../types";

export default async (rfc: string): Promise<User> => {
  const query = `
  select *
  from persona as p
  join usuario as u 
    on u.persona_id = p.id
  where p.rfc = :rfc
  `;
  const res = await db.raw(query, { rfc });
  return res.rowCount > 0 ? res.rows[0] : null;
};
