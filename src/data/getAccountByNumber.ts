import db from "./db";

export default async (numero: number) => db("cuenta").first().where({ numero });
