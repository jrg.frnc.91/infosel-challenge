import db from "./db";

export default async (
  userId: number,
  accountNumber: string
): Promise<boolean> => {
  const query = `
  select exists (
    select *
    from cuenta as c
    join persona as p 
      on c.persona_id = p.id
    join usuario as u 
      on u.persona_id = p.id
    where u.id = :userId
    and c.numero = :accountNumber
  )
  `;
  const res = await db.raw(query, { userId, accountNumber });
  return res.rows[0].exists;
};
