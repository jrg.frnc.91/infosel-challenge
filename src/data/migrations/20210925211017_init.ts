import { Knex } from "knex";

export async function up(knex: Knex): Promise<void> {
  return knex.schema
    .createTable("persona", (table: Knex.CreateTableBuilder) => {
      table.increments();
      table.text("nombres").notNullable();
      table.text("apellidos").notNullable();
      table.text("telefono");
      table.text("rfc").unique().notNullable();
      table.text("direccion");
    })
    .createTable("usuario", (table: Knex.CreateTableBuilder) => {
      table.increments();
      table.text("contrasena").notNullable().unique();
      table.boolean("estatus").notNullable();
      table.integer("persona_id").references("id").inTable("persona");
    })
    .createTable("producto", (table: Knex.CreateTableBuilder) => {
      table.increments();
      table.text("tipo").notNullable().unique();
    })
    .createTable("cuenta", (table: Knex.CreateTableBuilder) => {
      table.increments();
      table.text("numero").notNullable().unique();
      table.integer("producto_id").references("id").inTable("producto");
      table.bigInteger("saldo").notNullable();
      table.boolean("estado").notNullable();
      table.text("nip").notNullable();
      table.integer("persona_id").references("id").inTable("persona");
    })
    .createTable("movimiento", (table: Knex.CreateTableBuilder) => {
      table.increments();
      table.integer("cuenta_destino").references("id").inTable("cuenta");
      table.integer("cuenta_origen").references("id").inTable("cuenta");
      table.text("tipo_movimiento").notNullable();
      table.bigInteger("monto").notNullable();
      table.text("referencia");
      table.text("operacion");
      table.date("fecha_aplicacion").notNullable();
      table.text("estatus").notNullable();
    });
}

export async function down(knex: Knex): Promise<void> {
  return knex.schema
    .dropTableIfExists("movimiento")
    .dropTableIfExists("cuenta")
    .dropTableIfExists("producto")
    .dropTableIfExists("usuario")
    .dropTableIfExists("persona");
}
