import db from "./db";

export default async (accountNumber: string): Promise<boolean> => {
  const query = `
  select exists (
    select *
    from cuenta as c
    where c.numero = :accountNumber
  )
  `;
  const res = await db.raw(query, { accountNumber });
  return res.rows[0].exists;
};
