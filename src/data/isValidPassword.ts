import db from "./db";

export default async (rfc: string, password: string): Promise<boolean> => {
  const query = `
  select count(*)
  from persona as p
  join usuario as u 
    on u.persona_id = p.id
  where p.rfc = :rfc
  and contrasena = :password
  and u.estatus is true
  `;
  const res = await db.raw(query, { rfc, password });
  return Number(res.rows[0].count) > 0;
};
