import db from "./db";
import { Person } from "../types";

export default async (person: Person): Promise<number> => {
  const x = await db("persona").insert(person).returning("id");
  return x[0];
};
