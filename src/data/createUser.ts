import db from "./db";
import { User } from "../types";

export default async (user: User): Promise<number> => {
  const x = await db("usuario").insert(user).returning("id");
  return x[0];
};
