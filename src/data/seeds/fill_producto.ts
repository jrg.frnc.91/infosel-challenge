import { Knex } from "knex";

export async function seed(knex: Knex): Promise<void> {
  await knex("producto").del();

  await knex("producto").insert([
    { tipo: "chequera" },
    { tipo: "debito" },
    { tipo: "credito" },
  ]);
}
