import { Knex } from "knex";

const config: Knex.Config = {
  client: "pg",
  connection: process.env.DB_CONNECTION_STRING,
  migrations: {
    directory: "./migrations",
  },
  seeds: {
    directory: "./seeds",
  },
};

export default config;
