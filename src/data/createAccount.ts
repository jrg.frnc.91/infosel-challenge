import db from "./db";
import { Account } from "../types";

export default async (account: Account): Promise<number> => {
  const x = await db("cuenta").insert(account).returning("id");
  return x[0];
};
