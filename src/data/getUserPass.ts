import db from "./db";

export default async (rfc: string): Promise<string> => {
  const query = `
  select u.contrasena
  from persona as p
  join usuario as u 
    on u.persona_id = p.id
  where p.rfc = :rfc
  `;
  const res = await db.raw(query, { rfc });
  return res.rows[0]?.contrasena || "";
};
