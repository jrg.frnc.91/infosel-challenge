import { NextFunction, Request, Response, Router } from "express";
import bodyValidator from "../middlewares/bodyValidator";
import transferSchema from "../validation/transferSchema";
import auth from "../middlewares/auth";
import validateUserOwnsAccount from "../validateUserOwnsAccount";
import validateAccountExists from "../validateAccountExists";

type ReqBody = {
  fromAccount: string;
  toAccount: string;
  amount: number;
  concept: string;
  operation: string;
};

const r = Router();

/**
 * @swagger
 * /transfer:
 *   post:
 *     requestBody:
 *       required: true
 *       content:
 *         application/json:
 *           schema:
 *             type: object
 *             properties:
 *               fromAccount:
 *                 type: string
 *               toAccount:
 *                 type: string
 *               concept:
 *                 type: string
 *               operation:
 *                 type: string
 *               amount:
 *                 type: number
 *     responses:
 *       200:
 *         description: Created
 */
r.post(
  "/transfer",
  bodyValidator(transferSchema),
  auth,
  async (req: Request, res: Response, next: NextFunction) => {
    try {
      const { fromAccount, toAccount, amount, concept, operation } =
        req.body as ReqBody;
      const { userId } = req;
      await validateUserOwnsAccount(userId, fromAccount);
      await validateAccountExists(toAccount);
      res.send();
    } catch (e) {
      next(e);
    }
  }
);

export default r;
