import { NextFunction, Request, Response, Router } from "express";
import jwt from "jsonwebtoken";
import bodyValidator from "../middlewares/bodyValidator";
import loginSchema from "../validation/loginSchema";
import validatePassword from "../validatePassword";
import getUserByRfc from "../data/getUserByRfc";

type ReqBody = {
  rfc: string;
  contrasena: string;
};

const r = Router();

/**
 * @swagger
 * /login:
 *   post:
 *     requestBody:
 *       required: true
 *       content:
 *         application/json:
 *           schema:
 *             type: object
 *             properties:
 *               rfc:
 *                 type: string
 *               contrasena:
 *                 type: string
 *     responses:
 *       200:
 *         content:
 *           application/json:
 *             schema:
 *               type: object
 *               properties:
 *                    token:
 *                      type: string
 */
r.post(
  "/login",
  bodyValidator(loginSchema),
  async (req: Request, res: Response, next: NextFunction) => {
    try {
      const { rfc, contrasena: plainTextPass } = req.body as ReqBody;
      await validatePassword(rfc, plainTextPass);
      const user = await getUserByRfc(rfc);
      const payload = { userId: user.id };
      const token = jwt.sign(payload, process.env.JWT_SECRET as jwt.Secret);
      res.send({ token });
    } catch (e) {
      next(e);
    }
  }
);

export default r;
