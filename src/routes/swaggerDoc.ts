import { Router } from "express";
import swaggerUi from "swagger-ui-express";
import swaggerJSDoc from "swagger-jsdoc";

const swaggerDefinition = {
  openapi: "3.0.0",
  info: {
    title: "Infosel challenge API",
    version: "1.0.0",
  },
  servers: [
    {
      url: `http://localhost:${process.env.PORT}`,
      description: "Server",
    },
  ],
};

const options = {
  swaggerDefinition,
  apis: ["./src/routes/*.ts"],
};

const swaggerSpec = swaggerJSDoc(options);

const r = Router();

r.use("/docs", swaggerUi.serve, swaggerUi.setup(swaggerSpec));

export default r;
