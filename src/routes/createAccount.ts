import { NextFunction, Request, Response, Router } from "express";
import bodyValidator from "../middlewares/bodyValidator";
import createAccount from "../data/createAccount";
import createAccountSchema from "../validation/createAccountSchema";
import createPerson from "../data/createPerson";
import createUser from "../data/createUser";
import createAccountNumber from "../createAccountNumber";
import hashPassword from "../utils/hashPassword";

type ReqBody = {
  nombres: string;
  apellidos: string;
  rfc: string;
  contrasena: string;
  nip: string;
};

const r = Router();

/**
 * @swagger
 * /create-account:
 *   post:
 *     requestBody:
 *       required: true
 *       content:
 *         application/json:
 *           schema:
 *             type: object
 *             properties:
 *               nombres:
 *                 type: string
 *               apellidos:
 *                 type: string
 *               rfc:
 *                 type: string
 *               contrasena:
 *                 type: string
 *               nip:
 *                 type: string
 *     responses:
 *       200:
 *         description: Created
 */
r.post(
  "/create-account",
  bodyValidator(createAccountSchema),
  async (req: Request, res: Response, next: NextFunction) => {
    try {
      const {
        nombres,
        apellidos,
        rfc,
        contrasena: plainTextPass,
        nip,
      } = req.body as ReqBody;
      const personId = await createPerson({ nombres, apellidos, rfc });
      await createUser({
        persona_id: personId,
        contrasena: await hashPassword(plainTextPass),
        estatus: true,
      });
      await createAccount({
        estado: true,
        nip,
        numero: await createAccountNumber(),
        persona_id: personId,
        producto_id: 2, // debito
        saldo: 0,
      });
      res.send();
    } catch (e) {
      next(e);
    }
  }
);

export default r;
