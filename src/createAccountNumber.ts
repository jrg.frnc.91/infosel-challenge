import randomInclusiveNumber from "./utils/randomInclusiveNumber";
import getAccountByNumber from "./data/getAccountByNumber";

export default async () => {
  let isUnique = false;
  let n: number;
  do {
    n = randomInclusiveNumber(1000000000, 9999999999);
    // eslint-disable-next-line no-await-in-loop
    if (!(await getAccountByNumber(n))) {
      isUnique = true;
    }
  } while (!isUnique);
  return String(n);
};
