import createError from "http-errors";
import userOwnsAccount from "./data/userOwnsAccount";

export default async (userId: number, accountNumber: string): Promise<void> => {
  if (!(await userOwnsAccount(userId, accountNumber))) {
    throw createError(400, "User does not own account");
  }
};
