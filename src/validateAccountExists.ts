import createError from "http-errors";
import accountExists from "./data/accountExists";

export default async (accountNumber: string): Promise<void> => {
  if (!(await accountExists(accountNumber))) {
    throw createError(400, "account does not exist");
  }
};
