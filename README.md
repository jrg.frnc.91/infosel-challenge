# Infosel challenge
This is a REST API implemented using TypeScript + express.

## Configuration
A *.env* file must be created at the root of the folder.
```
NODE_ENV=production|development # if ommited, production is set
PORT=0000 # if ommited, 3000 is set
DB_CONNECTION_STRING=postgresql://[user[:password]@][netloc][:port][/dbname][?param1=value1&...]
JWT_SECRET=asdfasdfasdf # secret used to encode and decode jwt
```

## How to run
This project requires node 14 or later.

Make sure the *.env* file exists at the root before doing anything!

Install deps
```bash
npm i
```

Build the app
```bash
npm run build
```

Run the app
```bash
npm start
```

If wanted, a jwt secret can be created and later put in the *.env* file
```bash
npm run create-jwt-secret
```

Migrate and seed the db for the first time
```bash
npm run migrate
npm run seed
```
